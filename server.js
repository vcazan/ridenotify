// server.js

// set up ======================================================================
// get all the tools we need
var express = require('express');
var app = express();
var port = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var http = require('http');
var restbus = require('restbus');

var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var configDB = require('./config/database.js');

var mongojs = require("mongojs");

var uri = "mongodb://ride:ride@ds061757.mongolab.com:61757/ridenotify";
var collections = ["users", "stops"]
var db = mongojs.connect(uri, collections);

// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({
    extended: true
}));

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({
    secret: 'ilovescotchscotchyscotchscotch'
})); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
app.use(express.static((__dirname, 'public')));

// RESTBUS SERVER START =========================================================
restbus.listen(function () {
    console.log('restbus is now listening on port 3535');
});

// launch ======================================================================
var server = app.listen(port);
var io = require('socket.io').listen(server);

io.set('log level', 1);

io.on('connection', function (socket) {
    socket.on('listRoutes', function (data) {
        //console.log(data);

        var Client = require('node-rest-client').Client;
        client = new Client();

        client.get("http://localhost:3535/agencies/" + data + "/routes", function (data, response) {
            // parsed response body as js object
            //console.log(data);

            data = JSON.parse(data);
            var routes = [];
            for (var i = 0; i < data.length; i++) {
                //console.log( data[i].id);
                routes.push({
                    id: data[i].id,
                    title: data[i].title
                });
            }
            socket.emit('routes', routes);


        });
    });
    socket.on('listStops', function (data) {
        //console.log(data);

        var Client = require('node-rest-client').Client;
        client = new Client();

        client.get("http://localhost:3535/agencies/" + data.agency + "/routes/" + data.route, function (data, response) {
            // parsed response body as js object


            data = JSON.parse(data);
            data = data.stops;
            var stops = [];
            for (var i = 0; i < data.length; i++) {
                //console.log( data[i].id);
                stops.push({
                    id: data[i].id,
                    title: data[i].title
                });
            }
            socket.emit('stops', stops);


        });
    });
    socket.on('showStop', function (datas) {
        //console.log(data);

        var Client = require('node-rest-client').Client;
        client = new Client();

        client.get("http://localhost:3535/agencies/" + datas.agency + "/routes/" + datas.route, function (data, response) {
            // parsed response body as js object


            data = JSON.parse(data);
            data = data.stops;
            var stop = [];
            for (var i = 0; i < data.length; i++) {
                //console.log( data[i].id);
                if (data[i].id == datas.stop) {
                    //console.log("Found Stop");
                    stop.push({
                        id: data[i].id,
                        title: data[i].title,
                        lat: data[i].lat,
                        lon: data[i].lon,
                        code: data[i].code
                    });
                }
            }
            //console.log(stop);
            socket.emit('stop', stop);


        });
    });

    socket.on('saveStop', function (datas) {
        db.stops.save(datas, function (err, saved) {
            if (err || !saved) console.log("Stop not saved");
            else console.log("Stop saved");
        });
    });
    
    socket.on('delRoute', function (datas) {
        
        var sid = datas.toString();
        var mongojs = require('mongojs');
        var ObjectId = mongojs.ObjectId;

        db.stops.remove({_id: ObjectId(datas)}, function (err, saved) {
            if (err || !saved) console.log("Stop not deleted");
            else console.log("Stop deleted");
        });
        socket.emit('deleted');

    });
});

console.log('RideNotify server started on port ' + port);

// routes ======================================================================
require('./app/routes.js')(app, passport, io, db, mongojs); // load our routes and pass in our app and fully configured passport