// define the schema for our user model
var stopsSchema = mongoose.Schema({

    stop            : {
        userid        : String,
        stopcode     : String,
    }

});

stopsSchema.methods.save = function (id,code) {
    return 'saved';
}
// create the model for stops and expose it to our app
module.exports = mongoose.model('Stops', stopsSchema);
